=== Skyword Publishing API ===
Contributors: skyword
Tags: skyword, api
Requires at least: 3.3
Tested up to: 5.0.3
Stable tag: 1.0.7

Allows integration with the Skyword360 publishing platform.

== Description ==

[Skyword360](http://www.skyword.com/) is the leading comprehensive content production platform. Skyword360 enables brands, retailers, media companies, and agencies to acquire and engage customers by efficiently producing quality content optimized for search and the social web. Skyword integrates its clients' WordPress sites with the Skyword360 platform using this REST implementation of its Publishing API. With this integration, Skyword360 makes it easy to produce, optimize, and promote content at any scale to create meaningful, lasting relationships with customers. Contact Skyword at https://www.skyword.com/contact-us/.

== Installation ==

== Frequently Asked Questions ==

== Screenshots ==

== Changelog ==

= 1.0.7 =
* Initial release
